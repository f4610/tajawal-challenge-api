# Tajwal Challenge
## Tools

- Newman ([postman terminal version](https://github.com/postmanlabs/newman))
- Reporting [newman-reporter-htmlextra](https://nodejs.org/) 
- Gitlab CI/CD 

### why the stack above?
##### Newman

Newman is one of the fastest and robust api automation tool in the market. Cypress has also the possibility to use "Cypress Requests" to run API automation. But the issue is that api automation on cypress run over browsers

#### Newman Reporter html extra

Newman Reporter html extra generate a very detailed api report which allow us to debug and see any request details

#### GitLab CI/CD

Gitlab give us the possibility to push our docker images, run testing and works as a repository. 

---
## Scenarios
##### Dynamic run:
Generates random data for:
- checkin
- checkout 
- guests array random from 1 to 4
- Sets automatic if the guest is adult(ADT) or chield (CHD) Where if the guest is child also sets randonly age from 0 up to 8
 
The given scenario was created to cover the requested items below:

- Dates should be dynamic
- Room and guest array should be created dynamically
- Perform needed assertions on the API response
- Report generation for executed scripts

##### Check if api blocks empty checkin / checkout::

The given scenario validate if the API do not allow users to enter checkin dates "smaller" than today. 

##### Validate checkout accepts only from day after:

The scenario validate if the API do not allow users to enter checkout dates "smaller" than day after. 

##### Check if api blocks empty checkin / checkout:

The scenario checks if the user can enter empty values. Currently API allows the user to enter checkin and checkout empty which probably is expected, looks like it enters 14 days after as checkin and 15 days as checkout (from today). Just to have a failing scenario the assertion fails if the checkin and checkout are empty.

The intent of those scenarios below:

- Check if api blocks empty checkin / checkout:
- Validate checkout accepts only from day after
- Check if api blocks empty checkin / checkout

are to simulate some business rules / acceptance criteria as we have in a "real life" software development life cycle 

## Running Tests

Install newman

```sh
npm install -g newman 
```

Clone the project then, run the given command

```sh
newman run Tajwal-Api.postman_collection.json
```

## Test report

When the test is loaded over gitlab ci/cd, a report will be generated. An example may be found [here](https://f4610.gitlab.io/-/tajawal-challenge-api/-/jobs/1526483803/artifacts/report.html)

## Docker

Each time some change happen in the project a docker image is created and pushed to gitlab docker containers found [here](https://gitlab.com/f4610/tajawal-challenge-api/container_registry/2221124)

## Development GitFlow model

This repository uses GitFlow model therefore develop branch is the default
